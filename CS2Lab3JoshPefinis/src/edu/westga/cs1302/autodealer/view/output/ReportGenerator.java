package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param inventory the inventory
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Inventory inventory) {
		String summary = "";
		
		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = inventory.getDealershipName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			NumberFormat decimalFormatter = new DecimalFormat("#0.000");
			
			Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
			Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();
			String averageMiles = decimalFormatter.format(inventory.getAverage());
			
			summary += System.lineSeparator();

			summary += "Most expensive auto: ";
			summary += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();
			summary += "Least expensive auto: ";
			summary += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();
			summary += "Average Miles: ";
			summary += averageMiles + System.lineSeparator() + System.lineSeparator();
			summary += this.buildSegmentSummary(inventory, 3000.00) + System.lineSeparator();
			summary += this.buildSegmentSummary(inventory, 10000.00) + System.lineSeparator();
		}

		return summary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

		String output = currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake() + " "
				+ auto.getModel();
		return output;
	}
	
	private String buildSegmentSummary(Inventory inventory, Double range) {
		Integer[] segments = inventory.getSegments(range);
		
		NumberFormat currencyFormatter =
			    NumberFormat.getCurrencyInstance(Locale.US);
		
		String summary = "Vehicles in " + currencyFormatter.format(range) 
				+ " price range segments" + System.lineSeparator();
		
		for (int index = 0; index < segments.length; index++) {
			
			double lowerLimit = range * index;
			double upperLimit = range * (index + 1);
			
			if (index != 0) {
				lowerLimit += 0.01;
			}
			
			summary += currencyFormatter.format(lowerLimit) + " - " 
					+  currencyFormatter.format(upperLimit) + " : " 
					+ segments[index] + System.lineSeparator();
		}
		
		return summary;
	}
}
