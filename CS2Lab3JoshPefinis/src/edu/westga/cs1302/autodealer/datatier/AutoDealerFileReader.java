package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file which is a
 * CSV file with the following format:
 * make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File inventoryFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}
		
		this.inventoryFile = inventoryFile;
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one
	 * line at a time. Parses each line and creates an automobile object and stores it in
	 * an ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all
	 * autos in the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 * @throws FileNotFoundException 
	 */
	public ArrayList<Automobile> loadAllAutos() throws FileNotFoundException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>();
		
		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				
				autos.add(this.arrayToAuto(this.splitLine(line, ",")));
			}
		}

		return autos;  
	}
	
	/**
	 * Converts a single line from an adi file into an array of strings.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return autoArray An Array of Strings representing the fields of an Automobile
	 */
	private String[] splitLine(String line, String delimiter) {
		Scanner input = new Scanner(line);
		input.useDelimiter(delimiter);
		
		String[] autoArray = {input.next(), input.next(), input.next(), input.next(), input.next()};
		
		input.close();
		
		return autoArray;
	}
	
	/**
	 * Creates an Automobile from a String[]
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return newAuto An Automobile
	 */
	private Automobile arrayToAuto(String[] autoArray) {
		String make = autoArray[0];
		String model = autoArray[1];
		int year = Integer.parseInt(autoArray[2]);
		double miles = Double.parseDouble(autoArray[3]);
		double price = Double.parseDouble(autoArray[4]);
		
		Automobile newAuto = new Automobile(make, model, year, miles, price);
		return newAuto;
	}

}
