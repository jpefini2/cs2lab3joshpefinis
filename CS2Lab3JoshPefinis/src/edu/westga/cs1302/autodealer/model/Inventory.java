package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class Inventory.
 * 
 * @author CS1302
 */
public class Inventory {
	private static final String START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR = "startYear must be <= endYear.";
	private static final String AUTO_CANNOT_BE_NULL = "auto cannot be null.";
	private static final String DEALERSHIP_NAME_CANNOT_BE_EMPTY = "dealershipName cannot be empty.";
	private static final String DEALERSHIP_NAME_CANNOT_BE_NULL = "dealershipName cannot be null.";

	private String dealershipName;
	private ArrayList<Automobile> autos;

	/**
	 * Instantiates a new inventory.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public Inventory() {
		this.dealershipName = null;
		this.autos = new ArrayList<Automobile>();
	}

	/**
	 * Instantiates a new inventory.
	 * 
	 * @precondition dealershipName cannot be null or empty
	 * @postcondition getDealershipName() == dealershipName AND size() == 0
	 *
	 * @param dealershipName the dealership name
	 */
	public Inventory(String dealershipName) {
		if (dealershipName == null) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_NULL);
		}

		if (dealershipName.isEmpty()) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_EMPTY);
		}

		this.dealershipName = dealershipName;
		this.autos = new ArrayList<Automobile>();
	}

	/**
	 * Numbers of autos in inventory.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of autos in the inventory.
	 */
	public int size() {
		return this.autos.size();
	}

	/**
	 * Adds the auto to the inventory.
	 * 
	 * @precondition auto != null
	 * @postcondition size() == size()@prev + 1
	 *
	 * @param auto the auto
	 * @return true, if add successful
	 */
	public boolean add(Automobile auto) {
		if (auto == null) {
			throw new IllegalArgumentException(AUTO_CANNOT_BE_NULL);
		}

		return this.autos.add(auto);
	}

	/**
	 * Adds all the autos to the inventory.
	 * 
	 * @precondition autos != null
	 * @postcondition size() == size()@prev + autos.size()
	 *
	 * @param autos the autos to add to the inventory
	 * 
	 * @return true, if add successful
	 */
	public boolean addAll(ArrayList<Automobile> autos) {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		return this.autos.addAll(autos);
	}

	/**
	 * Find year of oldest auto.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the oldest auto or Integer.MAX_VALUE if no autos
	 */
	public int findYearOfOldestAuto() {
		int oldest = Integer.MAX_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() < oldest) {
				oldest = automobile.getYear();
			}
		}

		return oldest;
	}

	/**
	 * Find year of newest auto.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the newest auto or Integer.MIN_VALUE if no autos
	 */
	public int findYearOfNewestAuto() {
		int newest = Integer.MIN_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() > newest) {
				newest = automobile.getYear();
			}
		}

		return newest;
	}

	/**
	 * Find most expensive auto in the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the most expensive automobile or null if no autos in
	 *         inventory.
	 */
	public Automobile findMostExpensiveAuto() {
		Automobile maxAuto = null;
		double maxPrice = 0;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() > maxPrice) {
				maxAuto = currAuto;
				maxPrice = maxAuto.getPrice();
			}
		}

		return maxAuto;
	}

	/**
	 * Find least expensive auto in the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the least expensive automobile or null if no autos in
	 *         inventory.
	 */
	public Automobile findLeastExpensiveAuto() {
		Automobile minAuto = null;
		double minPrice = Double.MAX_VALUE;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() < minPrice) {
				minAuto = currAuto;
				minPrice = minAuto.getPrice();
			}
		}

		return minAuto;
	}

	/**
	 * Count autos between specified start and end year inclusive.
	 *
	 * @precondition startYear <= endYear
	 * @postcondition none
	 * 
	 * @param startYear the start year
	 * @param endYear   the end year
	 * @return the number of autos between start and end year inclusive.
	 */
	public int countAutosBetween(int startYear, int endYear) {
		if (startYear > endYear) {
			throw new IllegalArgumentException(START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR);
		}
		int count = 0;
		for (Automobile automobile : this.autos) {
			if (automobile.getYear() >= startYear && automobile.getYear() <= endYear) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Deletes the specified auto from the inventory.
	 * 
	 * @precondition none
	 * @postcondition if found, size() == size()@prev � 1
	 * @param auto The automobile to delete from the inventory.
	 * 
	 * @return true if the auto was found and deleted from the inventory, false
	 *         otherwise
	 */
	public boolean remove(Automobile auto) {
		return this.autos.remove(auto);
	}

	/**
	 * Gets the dealership name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the dealershipName
	 */
	public String getDealershipName() {
		return this.dealershipName;
	}

	/**
	 * Sets the dealership name.
	 * 
	 * @precondition dealershipName cannot be null or empty
	 * @postcondition getDealershipName() == dealershipName
	 *
	 * @param dealershipName the dealershipName to set
	 */
	public void setDealershipName(String dealershipName) {
		if (dealershipName == null) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_NULL);
		}

		if (dealershipName.isEmpty()) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_EMPTY);
		}

		this.dealershipName = dealershipName;
	}

	/**
	 * Gets the autos.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the autos
	 */
	public ArrayList<Automobile> getAutos() {
		return this.autos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Dealership:" + this.dealershipName + "#autos:" + this.size();
	}
	
	/**
	 * Finds the average miles of all automobiles in the inventory
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return average the average miles
	 */
	public double getAverage() {
		double sum = 0.0;
		
		for (Automobile current : this.autos) {
			sum = sum + current.getMiles();
		}
		
		double average = sum / this.autos.size();
		return average;
	}
	
	/**
	 * Finds the number of Automobiles within price segments of specified sizes
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param segmentRange The range of each segment
	 * 
	 * @return Integer[] The number of Automobiles in each segment
	 */
	public Integer[] getSegments(double segmentRange) {
		double highestPrice = this.findMostExpensiveAuto().getPrice();
		Double rawSegmentAmount = highestPrice / segmentRange;
		int segmentAmount = rawSegmentAmount.intValue();
		
		if (segmentAmount < rawSegmentAmount) {
			segmentAmount++;
		}
		
		Integer[] autoSegments = new Integer[segmentAmount];
		
		double lowerLimit = 0;
		double upperLimit = segmentRange;
		
		for (int index = 0; index < segmentAmount; index++) {	
			int count = 0;
			for (Automobile current : this.autos) {
				
				if (lowerLimit < current.getPrice() && current.getPrice() < upperLimit) {
					count++;
				}
			}
			
			autoSegments[index] = count;
			
			if (index == 0) {
				lowerLimit += 0.01;
				
			}
			
			lowerLimit += segmentRange;
			upperLimit += segmentRange;
		}
		return autoSegments;
	}
}
